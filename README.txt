********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Node Triggers Module
Author: Patrick Fournier <pfournier at whiskyechobravo dot com>
Drupal: 5
********************************************************************
PREREQUISITES:
. Actions (http://drupal.org/project/actions)

********************************************************************
DESCRIPTION:

This module allows you to trigger actions (from the Actions module
or any other module that defines actions) when something happens
to a node (insertion, update, deletion).

You can choose which type of node will trigger an action.

For example, you can set up a trigger so that an action gets 
executed each time a Story node is created.


********************************************************************
INSTALLATION:

1. Place the entire nodetriggers directory into your Drupal modules
   directory (normally sites/all/modules or modules).

2. Enable the nodetriggers module by navigating to:

     Administer > Site building > Modules

3. If you want anyone besides the administrative user to be able
   to configure triggers (usually a bad idea), they must be given
   the "administer node triggers" access permission:
   
     Administer > User management > Access control

   When the module is enabled and the user has the "administer
   node triggers" permission, a "Node Triggers" menu should appear 
   under Administer > Site configuration in the menu system.

********************************************************************
